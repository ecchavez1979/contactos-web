package com.sinbugs.contacts.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.sinbugs.contacts.dto.Contact;


public interface ContactRepository extends JpaRepository<Contact, Long> {
	
	@Service
	public class ContactService {
	    @Autowired
	    ContactRepository dao;
	     
	    public Contact save(Contact contact){
	        return dao.saveAndFlush(contact);
	    }
	}

}